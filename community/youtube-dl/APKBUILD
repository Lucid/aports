# Contributor: Peter Bui <pnutzh4x0r@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Timo Teräs <timo.teras@iki.fi>
# Maintainer: Timo Teräs <timo.teras@iki.fi>
pkgname=youtube-dl
pkgver=2021.02.10
pkgrel=0
pkgdesc="Command-line program to download videos from YouTube"
url="https://youtube-dl.org/"
arch="noarch"
license="Unlicense"
depends="python3"
checkdepends="py3-flake8 py3-nose"
subpackages="$pkgname-doc
	$pkgname-zsh-completion:zshcomp
	$pkgname-bash-completion
	$pkgname-fish-completion"
source="https://youtube-dl.org/downloads/$pkgver/youtube-dl-$pkgver.tar.gz"
builddir="$srcdir/$pkgname"

prepare() {
	default_prepare
	sed -i \
		-e 's|etc/bash_completion.d|share/bash-completion/completions|' \
		-e 's|etc/fish/completions|share/fish/completions|' \
		"$builddir"/setup.py
}

build() {
	python3 setup.py build
}

check() {
	PYTHON=/usr/bin/python3 make offlinetest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

zshcomp() {
	pkgdesc="Zsh completions for $pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel zsh"

	install -Dm644 "$builddir"/$pkgname.zsh \
		"$subpkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="84de569e5fb691af84e0dca6de5912b99912df2d88b5933740dfa76ce57027b21e1648dd7c8fe71f40839297d670f1a0cefaa3c1fc4d0c6d9beddbd22d0e93ec  youtube-dl-2021.02.10.tar.gz"
