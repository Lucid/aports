# Maintainer: ungleich <foss@ungleich.ch>
pkgname=cri-o
pkgver=1.20.0
pkgrel=2
pkgdesc="OCI-based implementation of Kubernetes Container Runtime Interface"
url="https://github.com/cri-o/cri-o/"
arch="all !mips !mips64" # limited by go
license="Apache-2.0"
makedepends="go bash linux-headers gpgme-dev btrfs-progs-dev ostree-dev
libassuan-dev lvm2-dev glib-dev libgpg-error-dev gpgme-dev
eudev-dev libselinux-dev tzdata"
depends="runc cni-plugins skopeo podman iptables iproute2 conmon"
docdepends="go-md2man"
options="net !check"
subpackages="$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	$pkgname-contrib-cni:contrib_cni:noarch
	$pkgname-openrc"
source="https://github.com/cri-o/cri-o/archive/v$pkgver/cri-o-$pkgver.tar.gz
	crio.initd
	crio.logrotated"
builddir="$srcdir/$pkgname-$pkgver"

build() {
	export GOPATH="$srcdir" 
	export GOBIN="$GOPATH/bin"
	make PREFIX="/usr" CRICTL_CONFIG_DIR="/etc/crio"
}

check() {
	go test ./...
}

package() {
	make DESTDIR="$pkgdir" PREFIX="$pkgdir/usr" CRICTL_CONFIG_DIR="$pkgdir/etc/crio" install
	install -Dm755 "$srcdir"/crio.initd "$pkgdir"/etc/init.d/crio
#	install -Dm644 "crio.conf" "${pkgdir}/etc/crio/crio.conf"
	install -Dm644 crio-umount.conf "${pkgdir}/etc/crio/crio-umount.conf"

	install -D -m644 "$srcdir"/crio.logrotated \
		"$pkgdir"/etc/logrotate.d/crio

	install -Dm644 LICENSE "${pkgdir}/usr/share/licenses/$pkgname/LICENSE"
	# Suppress crio log error messages triggered if these don't exist.
	mkdir -p  "$pkgdir"/etc/crio/crio.conf.d/
	mkdir -p  "$pkgdir"/etc/containers/oci/hooks.d
	mkdir -p  "$pkgdir"/usr/share/containers/oci/hooks.d

}

contrib_cni() {
	pkgdesc="$pkgname contrib cni config files package"
	mkdir -p "$subpkgdir"/etc/cni/net.d
	cp "$builddir"/contrib/cni/*.conf "$subpkgdir"/etc/cni/net.d
}

cleanup_srcdir() {
	go clean -modcache
	default_cleanup_srcdir
}

sha512sums="9bc718d0f2e082947f592e3a26f75a8a05116defecff7270f058e47ca0d0ba0a2a581ee155df15d5466e6dff8049bd52abc883577d7b917a0342d2133649d46c  cri-o-1.20.0.tar.gz
29561e95398975748236217bbd9df64997f6e3de6c0555d007306bd0535895a648368385a13079eb7d52c06249a91980523a73b6563e86d0575d9cd9c3fa4ee9  crio.initd
1115228546a696eeebeb6d4b3e5c3152af0c99a2559097fc5829d8b416d979c457b4b1789e0120054babf57f585d3f63cbe49949d40417ae7aab613184bf4516  crio.logrotated"
